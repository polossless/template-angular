import { HomeComponent } from './root/home/home.component';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { Routes } from '@angular/router/src/config';
import { RootComponent } from 'app/root/root.component';
import { PageNotFoundComponent } from 'app/assets/error/page.not.found.component';

export const ROUTES: Routes = [
    {
        path: '',
        component: AppComponent,
        children: [
            {
                path: '',
                component: RootComponent,
                children: [
                    {
                        path: '',
                        component: HomeComponent,
                    }]
            }]
    },
    { path: '**', component: PageNotFoundComponent }

];

@NgModule({
    declarations: [],
    imports: [
        RouterModule.forRoot(ROUTES, { useHash: false })
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutesModule {
    constructor() {

    }
}