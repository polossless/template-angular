import { AppRoutesModule } from './app.routes-module';
// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HoldingModule } from './holding/holding.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CatalogModule } from './catalog/catalog.module';
import { AuthModule } from './auth/auth.module';
import { RootModule } from './root/root.module';

/**
 * Environment
 */
import { environment } from 'environments/environment';

/**
 * Routes
 */
import { ROUTES } from './app.routes';

/**
 * App is our top level component
 */
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { APP_BASE_HREF } from '@angular/common';

/**
 * Components
 */
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './assets/error/page.not.found.component';

/**
 * Services
 */
import { AppService } from './shared/services/app.service';
import { HttpCustomService } from './shared/http.custom.service';
import '../styles/styles.scss';

/**
 * Guards
 */
import { AppGuard } from './shared/services/app.guard';

/**
 * Interceptors
 */
import { AppInterceptor } from './shared/services/app.interceptor';
import { TranslationModule } from './shared/modules/translation.module';

import { removeNgStyles, createNewHosts, bootloader, createInputTransfer } from '@angularclass/hmr';

@NgModule({
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutesModule,
        TranslationModule,
        RootModule,
    ],
    bootstrap: [AppComponent],
    declarations: [
        AppComponent,
        PageNotFoundComponent
    ],
    exports: [],
    providers: [
        environment.ENV_PROVIDERS,
        { provide: APP_BASE_HREF, useValue: '/' },
        AppGuard,
        AppInterceptor,
        AppService,
        HttpCustomService,
    ],
})

export class AppModule {
    constructor(private appRef: ApplicationRef) {

    }
    hmrOnInit(store) {
        if (!store || !store.state) return;
        console.log('HMR store', store);
        console.log('store.state.data:', store.state.data)
        // inject AppStore here and update it
        // this.AppStore.update(store.state)
        if ('restoreInputValues' in store) {
            store.restoreInputValues();
        }
        // change detection
        this.appRef.tick();
        delete store.state;
        delete store.restoreInputValues;
    }
    hmrOnDestroy(store) {
        var cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
        // recreate elements

        store.disposeOldHosts = createNewHosts(cmpLocation)

        // inject your AppStore and grab state then set it on store
        // var appState = this.AppStore.get()
        store.state = { data: 'yolo' };
        // store.state = Object.assign({}, appState)
        // save input values
        store.restoreInputValues = createInputTransfer();
        // remove styles
        removeNgStyles();
    }
    hmrAfterDestroy(store) {
        // display new elements
        store.disposeOldHosts()
        delete store.disposeOldHosts;
        // anything you need done the component is removed
    }
}
