import { Injectable } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

@Injectable()
export class AppService {

    public url: string[];

    constructor(public router: Router,
                public route: ActivatedRoute) {
        this.startListenUrl();
    }

    public startListenUrl() {
        this.router.events.subscribe((e) => {
            if (e instanceof NavigationEnd) {
                this.url = e.url.split('/');
                this.url.shift();
            }
        });
    }
}
