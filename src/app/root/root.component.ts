import { Component, OnInit, } from '@angular/core';
import { HoldingService } from '../shared/services/holding.service';

@Component({
    selector: 'root-component',
    templateUrl: './root.component.html'
})
export class RootComponent implements OnInit {
    public ngOnInit() {
        console.log('Root component!');
    }
}
