import { Component, OnInit } from '@angular/core';
import { AppService } from '../shared/services/app.service';

@Component({
    selector: 'home-component',
    templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
    public ngOnInit() {
        console.log('Home component!');
    }
}
