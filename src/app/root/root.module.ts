import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from '../assets/header/header.component';
import { FooterComponent } from '../assets/footer/footer.component';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { RootComponent } from './root.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    RouterModule,
  ],
  declarations: [
    RootComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
  ],
  providers: [],
  exports: [],
  bootstrap: [],
  entryComponents: []
})
export class RootModule {

}
