import {
    Component, OnInit,
    ViewEncapsulation
} from '@angular/core';

@Component({
    selector: 'app',
    encapsulation: ViewEncapsulation.None,
    styleUrls: [],
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
    public ngOnInit() {
        console.log('App component!');
    }
}
